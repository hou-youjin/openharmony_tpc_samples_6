﻿## domelementtype单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/fb55/domelementtype) 进行单元测试

**单元测试用例覆盖情况**

### domelementtype

| 接口名                                           | 是否通过 | 备注 |
|-----------------------------------------------| -------- | ---- |
| isTag(elem: { type: ElementType; }): boolean  | pass     |      |
