# buffer单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/feross/buffer/tree/master/test/node)
进行单元测试

单元测试用例覆盖情况

|      接口名      |                    是否通过	                     |备注|
|:-------------:|:--------------------------------------------:|:---:|
|  allocUnsafe  |                     pass                     |       |
|     from      |                     pass                     |       |
|     alloc     |                     pass                     |       |
|     write     |                     pass                     |       |
|   toString    |                     pass                     |       |
|     slice     |                     pass                     |       |
|     write     |                     pass                     |       |
|   toString    |                     pass                     |       |
|  byteLength   |                     pass                     |       |
|     fill      |                     pass                     |       |
| writeUInt32LE |                     pass                     |       |
| writeUInt32BE |                     pass                     |       |
| writeDoubleLE |                     pass                     |       |
|    compare    |                     pass                     |       |
|    concat     |                     pass                     |       |
|   includes    |                     pass                     |       |