/**
 *  MIT License
 *
 *  Copyright (c) 2024 Huawei Device Co., Ltd.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

import hilog from '@ohos.hilog';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium';
import { Buffer } from 'buffer/';

export default function comparseOffestTest() {
  describe('comparseOffestTest', () => {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })
    const a = Buffer.from([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);
    const b:ESObject = Buffer.from([5, 6, 7, 8, 9, 0, 1, 2, 3, 4]);
    it('test01', 0, () => {
      expect(-1).assertDeepEquals(a.compare(b))
      // Equivalent to a.compare(b).
      expect(-1).assertDeepEquals(a.compare(b, 0))
      expect(-1).assertDeepEquals(a.compare(b, undefined))
    })
    it('test02', 0, () => {
      // Equivalent to a.compare(b).
      expect(-1).assertDeepEquals(a.compare(b, 0, undefined, 0))
      // Zero-length target, return 1
      expect(1).assertDeepEquals(a.compare(b, 0, 0, 0))

      // Equivalent to Buffer.compare(a, b.slice(6, 10))
      expect(1).assertDeepEquals(a.compare(b, 6, 10))
      // Zero-length source, return -1
      expect(-1).assertDeepEquals(a.compare(b, 6, 10, 0, 0))
      // Zero-length source and target, return 0
      expect(0).assertDeepEquals(a.compare(b, 0, 0, 0, 0))
      expect(0).assertDeepEquals(a.compare(b, 1, 1, 2, 2))
      // Equivalent to Buffer.compare(a.slice(4), b.slice(0, 5))
      expect(1).assertDeepEquals(a.compare(b, 0, 5, 4))
      // Equivalent to Buffer.compare(a.slice(1), b.slice(5))
      expect(1).assertDeepEquals(a.compare(b, 5, undefined, 1))
      // Equivalent to Buffer.compare(a.slice(2), b.slice(2, 4))
      expect(-1).assertDeepEquals(a.compare(b, 2, 4, 2))
      // Equivalent to Buffer.compare(a.slice(4), b.slice(0, 7))
      expect(-1).assertDeepEquals(a.compare(b, 0, 7, 4))
      // Equivalent to Buffer.compare(a.slice(4, 6), b.slice(0, 7));
      expect(-1).assertDeepEquals(a.compare(b, 0, 7, 4, 6))
      // zero length target
      expect(1).assertDeepEquals(a.compare(b, 0, null))
    })
  })
}