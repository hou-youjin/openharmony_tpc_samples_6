# text-encoding单元测试用例

单元测试用例覆盖情况

|                            接口名                            |是否通过	|备注|
|:---------------------------------------------------------:|:---:|:---:|
|                           mark                            |pass   |        |
|                           wrap                            |pass   |        |
|                    isGeneratorFunction                    |pass   |        |
|                          values                           |pass   |        |

