/**
 * MIT License
 *
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import sanitize from 'sanitize-html';
import prompt from '@ohos.prompt';


@Entry
@Component
struct Allowed_schemes {
  private default_allows: string[] = sanitize.defaults.allowedSchemes;
  @State private schemes: string = '';
  @State private sanitizeResult: string = 'sanitizeResult: ';

  build() {
    Row() {
      Column({ space: 10 }) {
        Text(this.default_allows.toString())
          .fontSize(25)
          .fontWeight(FontWeight.Bold)

        TextInput({ placeholder: 'input schemes name.', controller: new TextInputController() })
          .onChange((value: string) => {
            this.schemes = value;
          })

        Button('sanitize html')
          .height('5%')
          .onClick(() => {
            if (!this.schemes) {
              prompt.showToast({ message: 'please input tag name' })
              return;
            }
            let html = '<a href="' + this.schemes + '://www.baidu.com">inner text </a>';
            this.sanitizeResult = 'sanitizeResult: ' + sanitize(html);
          })

        Button('Cover all default allowed scheme')
          .height('5%')
          .onClick(() => {
            if (!this.schemes) {
              prompt.showToast({ message: 'please input scheme' })
              return;
            }
            sanitize.defaults.allowedSchemes = ['http1', 'https1', 'sms1'];
            let html = '<a href="' + this.schemes + '://www.baidu.com">inner text </a>';
            this.sanitizeResult = 'sanitizeResult: ' + sanitize(html);
          })

        Button('Cover allowed scheme by options')
          .height('5%')
          .onClick(() => {
            if (!this.schemes) {
              prompt.showToast({ message: 'please input scheme' })
              return;
            }
            let html = '<a href="' + this.schemes + '://www.baidu.com">inner text </a>';
            this.sanitizeResult = 'sanitizeResult: ' + sanitize(html, {
              allowedSchemes: ['http2', 'https2', 'sms2']
            });
          })

        Button('add scheme for default')
          .height('5%')
          .onClick(() => {
            if (!this.schemes) {
              prompt.showToast({ message: 'please input scheme' })
              return;
            }
            let schemesArray:ESObject = sanitize.defaults.allowedSchemes;
            let index:ESObject = schemesArray.indexOf(this.schemes);
            if (index === -1) {
              schemesArray.push(this.schemes);
            } else {
              prompt.showToast({ message: 'the scheme already exist' })
              return;
            }

            let html = '<a href="' + this.schemes + '://www.baidu.com">inner text </a>';
            this.sanitizeResult = 'sanitizeResult: ' + sanitize(html);
          })

        Text(this.sanitizeResult)
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
      }
      .width('100%')
    }
    .height('100%')
  }
}