### 1.0.0

1. 样例适配的系统版本是 OpenHarmony API 9.
2. 使用三方库 behaviortree 版本支持@3.0.0-beta.1.
3. js版行为树功能样例的实现.

### 2.0.0

1. 适配DevEco Studio: 3.1 Beta1(3.1.0.400)
