/* jslint node:true */

import xml2js from './xml2js'
import xml2json from './xml2json';
import js2xml from './js2xml';
import json2xml from './json2xml';

export default {
  xml2js: xml2js,
  xml2json: xml2json,
  js2xml: js2xml,
  json2xml: json2xml
};
